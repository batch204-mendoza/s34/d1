// require directive is used to load the express module/packages
// it allows us to acces the methods and functions in easyily creating our server
const express= require("express");

// this creates an express application and stores  this in a constaled called app. app is our server

const app = express();

// setup for allowing serve to handledata from the requests
// methods used express JS are middle ware 
app.use(express.json());// allows your app to read json data.

//for our appliation server to run, we need a port to listen to the requests
const port =3000;
//express has methods corresponding to each http method. 
// "/" corresponds with our base URI
// localhost: 3000/
app.get("/", (req, res)=>{
	// res.send uses the the express.js modules method to send  a response back to the client. 
	res.send("hello World");

});
// this route expect to recieve a POST request at the uri endpoint "/hello"
app.post("/hello", (req, res)=>{

	// req.body containes the contents/ data of the request.
	console.log(req.body)
	res.send(`hello there ${req.body.firstName} ${req.body.lastName}`);
})

/*SCENARIO:

		We want to create a simple users database that will perform CRUD operations based on the client request. The following routes should peform its functionality:
*/

// mock database for our users
let users =[
	{
		"username" : "janedoe",
		"password": "jane123"
	},
	{
		"username" : "johnsmith",
		"password": "john123"
	}

];

// this route expects to receive a POST request at the URI "/signup"

// this will create a user object in the "users" array that mirrors a real world registration:
	// all fields are required

app.post("/signup", (req,res)=>{
//common practice to validate what was put in
	console.log(req.body);
	if(req.body.username !== "" 
		&& req.body.password !== ""
		&& req.body.password !== undefined
		&& req.body.username !== undefined){
		// to store the req.body sent via postman in the users array we will use the "push method"
		// user object will be saved in our users array.
		users.push(req.body);
		res.send(`user${req.body.username} succesfully registered!`);
	}
	else{
		res.send("please input both username and password");

	}

})
// this route expects tor recieve a get requet at uri 'users'
// app.get("/users", (req,res) => {
// 	res.send(users);
// }
app.get("/users", (req, res)=>{
		res.send(users);
	});
app.put("/change-password", (req, res)=>{
//creates a variable to store the message to be sent back to the client /postman (response)
let message;

//create a for loop that will loop through the elements of the "user" array
for (let i=0; i<users.length; i++){
	//if the username provided in the client/postman and the username of the current element/ object in the loop is the same
	if(users[i].username=== req.body.username){
		//changes the password of the user found by the loop into the updated password provided in the client/ postman. 
		users[i].password = req.body.password

		message= `user ${req.body.username}'s password has been updated.`
		// break out of the loop once the username provided matched.
		break;

	}
	// if no user was found
	else {
		//change the message to be sent bac as the response
		message="user does not exist!";
	}
}
//send a response back to the client/ postman once the password has been updated or if a user is not found. 
res.send(message);	
})

// app end
app.delete("/delete-user", (req, res)=>{
let message;


for (let i=0; i<users.length; i++){
	
	if(users[i].username=== req.body.username){
		 
		users.splice(users[i],1)

		message= `user ${req.body.username} has been deleted.`
		// break out of the loop once the username provided matched.
		break;

	}
	// if no user was found
	else {
		//change the message to be sent bac as the response
		message="user does not exist!";
	}
}
//send a response back to the client/ postman once the password has been updated or if a user is not found. 
res.send(message);	
})

// app end
//this route expect a to receive a put request at the URI "/change-password"
// this will update the password of the user that matches the information provide in the client /postman
	// we will use the "username" as the search property

//returns a message to confirm that the server is running in the terminal.
app.listen(port, () =>console.log(`server is running at port: ${port}`));


//